const express = require('express');
const path = require('path');
const fs = require('fs')
const app = express();
const port = 8080

fs.readFile('./files/file', 'utf8', (err, date ) => {
  console.log(date)
})

app.use(express.json())

app.post('/api/files', (req, res) => {
  const filename = req.body.filename
  const content = req.body.content
  fs.writeFile(filename, content,function (err) {
    if (err) throw err;
    console.log('File is created successfully.');
  });
  res.send(console.log(`Body: filename: ${filename} and content: ${content}`))
})

app.get('/api/files', (req, res) => {
  let fileArray = fs.readdir('.', (err, files) => {
    return files.map(file => file)
  });
  console.log(`files2: ${fileArray}`)
  res.json(fileArray);
  res.send(console.log(`hello from get: ${fileArray}`))
})

app.get('/api/files/:filename', (req, res) => {
  const filename = req.params.filename
  res.send(console.log(`hello from get ${filename}`))
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
